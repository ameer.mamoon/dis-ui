
class ApiConst{

  //static const _base = 'http://127.0.0.1:8000/api';

  static const _base = 'http://127.0.0.1:8000';

  static String autoComplete(String query) => '$_base/auto-complete?query=$query';

  static String autoSuggestion(String query) => '$_base/query-suggestion?query=$query';

  static String getResult(String query) => '$_base/get-result?query=$query';

  static String spellCorrection(String query) => '$_base/spell-correction?query=$query';

  static const changeDataset = '$_base/change-dataset';

}