
import 'package:dis/api_controller.dart';
import 'package:dis/search_delegate.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchController extends GetxController{

  String _dataset = 'none';


  String get dataset => _dataset;

  set dataset(String value) {
    _dataset = value;
    update();
  }

  void setDataset() async{
    var context = Get.context!;
    if(dataset != 'none') {
      Get.dialog(const Center(
        child: CircularProgressIndicator(),
      ));
      var res = await handle<bool>(() => ApiController.changeDataset(dataset));
      Get.back();
      if(res != null) {
        showSearch(context: context, delegate: MySearchDelegate());
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('select data set!!'))
      );
    }
  }


}

typedef _fun<T> = Future<T> Function();

Future<T?> handle<T>(_fun<T> future) async {
  try {
    final result = await future();
    return result;
  } catch (e) {
    if(Get.context != null)
    ScaffoldMessenger.of(Get.context!).showSnackBar(SnackBar(content: Text(e.toString())));
    return null;
  }
}