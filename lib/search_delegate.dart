import 'dart:math';
import 'package:dis/api_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MySearchDelegate extends SearchDelegate {
  @override
  Widget buildSuggestions(BuildContext context) => Column(
        children: [
          Expanded(
              child: DefaultTextStyle(
            style: const TextStyle(fontSize: 20, color: Colors.black),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: const [
                Text('Auto Complete'),
                Text('Queries'),
              ],
            ),
          )),
          Expanded(
              flex: 9,
              child: Row(
                children: [
                  Expanded(
                      child: FutureBuilder<List<String>>(
                    future: ApiController.autoComplete(query),
                    builder: (c, s) {
                      if (s.hasData) {
                        return ListView.separated(
                          itemCount: s.data!.length,
                          itemBuilder: (context, i) => ListTile(
                            title: Text(s.data![i]),
                            leading: const Icon(Icons.text_format_sharp),
                            onTap: () => query = s.data![i],
                          ),
                          separatorBuilder: (c, i) => const Divider(),
                        );
                      } else if (s.hasError) {
                        return Center(
                          child: Text(s.error.toString()),
                        );
                      } else {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                  )),
                  const VerticalDivider(),
                  Expanded(
                      child: FutureBuilder<List<String>>(
                    future: ApiController.autoSuggestion(query),
                    builder: (c, s) {
                      if (s.hasData) {
                        return ListView.separated(
                          itemCount: s.data!.length,
                          itemBuilder: (context, i) => ListTile(
                            title: Text(s.data![i]),
                            leading: const Icon(Icons.query_stats_outlined),
                            onTap: () => query = s.data![i],
                          ),
                          separatorBuilder: (c, i) => const Divider(),
                        );
                      } else if (s.hasError) {
                        return Center(
                          child: Text(s.error.toString()),
                        );
                      } else {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                  )),
                ],
              )),
        ],
      );

  @override
  List<Widget> buildActions(BuildContext context) => [
        IconButton(
            onPressed: () => query = '',
            icon: const Icon(Icons.backspace_outlined))
      ];

  @override
  Widget buildLeading(BuildContext context) => IconButton(
      onPressed: () => Navigator.pop(context),
      icon: const Icon(Icons.arrow_back_ios));

  @override
  Widget buildResults(BuildContext context) => Column(
        children: [
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(Get.size.width*0.005),
              child: FutureBuilder<String>(
                  future: ApiController.spellCorrection(query),
                  builder: (context,s){
                    if(s.hasData) {
                      return Row(
                        children: [
                          const Text(
                            'spell correction : ',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                                decoration: TextDecoration.underline,
                            ),
                          ),
                          InkWell(
                            onTap: ()=> query = s.data!,
                            child: Text(
                            s.data!,
                            style: TextStyle(
                              color: Colors.blue[600],
                              fontSize: 18
                            ),
                    ),
                          ),
                        ],
                      );
                    }
                    else if(s.hasError) {
                      return Row(
                        children: [
                          Text(s.error.toString(),
                          style: const TextStyle(
                              fontSize: 18,
                              color: Colors.red
                          ),),
                        ],
                      );
                    }
                    return DefaultTextStyle(
                      style: const TextStyle(
                        fontSize: 18,
                        color: Colors.black
                      ),
                      child: Row(
                        children: [
                          const Text('Spell correction is loading '),
                          StreamBuilder<String>(
                              stream: Stream.periodic(const Duration(
                                milliseconds: 300,
                              ),
                                (n) {
                                var res = '';
                                  for(int i = 0 ; i <= (n+1)%3 ; i++ )
                                    res += '.';
                                  return res;
                                },
                              ),
                              builder:(c,s) => Text(s.data ?? '')
                          )
                        ],
                      ),
                    );
                  }
              ),
            )
          ),
          Expanded(
            child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(child: _field('Doc ID', x: true)),
              Expanded(flex: 4, child: _field('Doc Name', x: true)),
              Expanded(child: _field('similarity', x: true)),
            ],
          )),
          Expanded(
              flex: 8,
              child: FutureBuilder<List<DocModel>>(
                future: ApiController.getResult(query),
                builder: (c, s) {
                  if (s.hasData) {
                    return ListView.separated(
                      itemCount: s.data!.length,
                      itemBuilder: (context, i) => Row(
                        children: [
                          Expanded(child: _field(s.data![i].docId.toString())),
                          Expanded(flex: 4, child: _field(s.data![i].text,x: true)),
                          Expanded(child: _field(s.data![i].score.toString())),
                        ],
                      ),
                      separatorBuilder: (c,i)=>const Divider(
                          color: Colors.black,
                        indent: 0,
                        height: 0,
                      ),
                    );
                  } else if (s.hasError) {
                    return Center(
                      child: Text(s.error.toString()),
                    );
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              )),
        ],
      );

  Widget _field(String text, {bool x = false}) => Container(
        decoration: BoxDecoration(
          border: x ? Border.all() : null,
        ),
        alignment: Alignment.center,
        child: Text(
          text,
          style: const TextStyle(
              fontSize: 18,
          ),
        ),
      );
}
