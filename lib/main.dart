import 'package:dis/search_controller.dart';
import 'package:dis/settings_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      routes: {
        '/':(context)=> SettingsScreen()
      },
      initialBinding: AppBinding(),
    );
  }
}

class AppBinding extends Bindings{

  @override
  void dependencies() {
    Get.put(SearchController(),permanent: true);
  }
}

