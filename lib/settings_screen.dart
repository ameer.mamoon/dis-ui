import 'package:dis/search_controller.dart';
import 'package:dis/search_delegate.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';



class SettingsScreen extends GetView<SearchController>{
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Select Data Set"),
        centerTitle: true,
      ),
      body: Padding(
          padding: EdgeInsets.symmetric(horizontal: size.width*0.25),
          child: SizedBox(
            width: double.infinity,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: size.height*0.2,
                  ),
                  GetBuilder<SearchController>(
                    builder: (_) {
                      return Text(
                        'data set : ${controller.dataset}',
                        style: TextStyle(
                            color: Colors.blue[700],
                            fontSize: 24,
                            fontWeight: FontWeight.w800
                        ),
                      );
                    }
                  ),
                  SizedBox(
                    height: size.height*0.2,
                  ),
                  ExpansionTile(
                    title: const Text("Tap to select"),
                    children: [
                      ListTile(
                          leading: const Icon(Icons.data_array),
                          title: const Text('qoura'),
                          onTap: () => controller.dataset = 'qoura'
                      ),
                      ListTile(
                          leading: const Icon(Icons.data_array),
                          title: const Text('antique'),
                          onTap: () => controller.dataset = 'antique'
                      ),
                    ],
                  ),
                  SizedBox(
                    height: size.height*0.2,
                  ),
                  ElevatedButton(
                      onPressed: controller.setDataset,
                      child: const Text('go search')
                  )
                ],
              ),
            ),
          )
      ),
    );
  }
}

