
import 'package:dio/dio.dart';
import 'package:dis/api_const.dart';

class ApiController{
  static final _dio = Dio();

  static Future<List<DocModel>> getResult(String query) async{
    var response = await _dio.get(ApiConst.getResult(query));
    return (response.data['data'] as List).map((e) => DocModel.fromJson(e)).toList();
  }

  static Future<List<String>> autoComplete(String query) async{
    var response = await _dio.get(ApiConst.autoComplete(query));
    return (response.data['data'] as List).map(
            (e) => (e as List).join(" ")
    ).toList();
  }

  static Future<List<String>> autoSuggestion(String query) async{
    var response = await _dio.get(ApiConst.autoSuggestion(query));
    return (response.data['data'] as List).map((e) => e.toString()).toList();
  }

  static Future<String> spellCorrection(String query) async{
    var response = await _dio.get(ApiConst.spellCorrection(query));
    return response.data['data'].toString();
  }

  static Future<bool> changeDataset(String dataset) async{
    await _dio.post(ApiConst.changeDataset,
    data: {
      "dataset_name":dataset
    },
      options: Options(
          headers:{
            'Content-Type':'application/json',
          }
      )
    );
    return true;
  }

}

class DocModel {
  final String docId;
  final String score;
  final String text;

  DocModel({
    required this.docId,
    required this.score,
    required this.text
  });

  factory DocModel.fromJson(Map<String,dynamic> json) => DocModel(
    docId: json['doc_id'].toString(),
    text: json['text'].toString(),
    score: json['score'].toString(),
  );
}